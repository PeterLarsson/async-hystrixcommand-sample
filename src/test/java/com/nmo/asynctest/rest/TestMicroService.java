package com.nmo.asynctest.rest;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


/**
 * Created by Peter on 2016-04-12.
 */
@RestController
@Slf4j
public class TestMicroService {

    @SneakyThrows
    @RequestMapping(value = "/{sleepTime}", method = RequestMethod.GET)
    public ResponseEntity<DataBean> sleepy(@PathVariable int sleepTime) {
        log.info("sleeping time is {}", sleepTime);
        final DataBean dataBean = new DataBean();
        dataBean.setName("test-" + sleepTime);
        dataBean.setBlog("N/A");
        dataBean.setLogin("login");
        Thread.sleep(sleepTime);
        return ResponseEntity.ok(dataBean);
    }

    @SneakyThrows
    @RequestMapping(value = "/products/{id}", method = RequestMethod.GET)
    public ResponseEntity<ProductBean> products(@PathVariable String id) {
        assertTestActions(id);
        final ProductBean productBean = new ProductBean();
        productBean.setId(id);
        productBean.setName("name-" + id);
        return ResponseEntity.ok(productBean);
    }

    @SneakyThrows
    @RequestMapping(value = "/profiles/{id}", method = RequestMethod.GET)
    public ResponseEntity<ProfileBean> profiles(@PathVariable String id) {
        assertTestActions(id);
        final ProfileBean profileBean = new ProfileBean();
        profileBean.setId(id);
        profileBean.setName("name-" + id);
        return ResponseEntity.ok(profileBean);
    }

    @SneakyThrows
    void assertTestActions(final String id) {
        switch (id) {
            case "error":
                throw new IllegalArgumentException(id);
            case "timeout":
                Thread.sleep(1001);
                break;
        }
    }
}
