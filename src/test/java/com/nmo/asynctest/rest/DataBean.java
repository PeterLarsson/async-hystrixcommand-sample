package com.nmo.asynctest.rest;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

/**
 * Created by Peter on 2016-04-12.
 */
@JsonIgnoreProperties(ignoreUnknown=true)
@Data
public class DataBean {
    private String login;
    private String name;
    private String blog;
}
