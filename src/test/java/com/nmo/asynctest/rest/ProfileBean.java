package com.nmo.asynctest.rest;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

/**
 * Created by Peter on 2016-04-14.
 */
@JsonIgnoreProperties(ignoreUnknown=true)
@Data
public class ProfileBean {
    private String id;
    private String name;
}
