package com.nmo.asynctest.rest;

import com.nmo.asynctest.TestSupport;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.mockito.internal.exceptions.ExceptionIncludingMockitoWarnings;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.async.DeferredResult;

import static org.junit.Assert.*;

/**
 * Created by Peter on 2016-04-17.
 */
@Slf4j
public class AsyncRestHystrixClientTest extends TestSupport {

    @Test
    public void successTest() {
        final Request<DataBean> request = request("1", DataBean.class);

        DeferredResult<ResponseEntity<DataBean>> deferredResult = runAync(request);
        DataBean dataBean = get(deferredResult).getBody();

        assertEquals("login", dataBean.getLogin());
    }


    @Test
    public void hystrixTimeoutNoFallbackTest() {
        final Request<DataBean> request = request("2000", DataBean.class);
        DeferredResult<ResponseEntity<DataBean>> deferredResult = runAync(request);

        assertFalse(get(deferredResult).getBody() instanceof DataBean);
    }

    @Test(expected = Exception.class)
    public void appErrorWhileLambdaProcessingResponse() {
        final Request<DataBean> request = request("1", DataBean.class);
        DeferredResult<ResponseEntity<DataBean>> deferredResult = asyncRestHystrixClient.execute(map -> {
            log.debug("response map: {}", map);
            DataBean dataBean = new DataBean();
            // null pointer
            dataBean.getName().toString();
            return new ResponseEntity<>(dataBean, HttpStatus.OK);
        }, request);

        //
        get(deferredResult);
    }

    @Test
    public void hystrixTimeoutWithFallbackTest() {
        final Request<DataBean> request = request("2000", DataBean.class);
        DataBean dataBean = new DataBean();
        dataBean.setName("fallback-bean");
        request.setFallbackValue(dataBean);

        DeferredResult<ResponseEntity<DataBean>> deferredResult = runAync(request);
        dataBean = get(deferredResult).getBody();
        assertNull(dataBean.getLogin());
    }

    @Test
    public void clientTimeoutWithFallbackTest() {
        final Request<DataBean> request = request("500", DataBean.class);
        DataBean dataBean = new DataBean();
        dataBean.setName("fallback-bean");
        request.setFallbackValue(dataBean);        request.setTimeout(10);

        DeferredResult<ResponseEntity<DataBean>> deferredResult = runAync(request);
        dataBean = get(deferredResult).getBody();
        assertNull(dataBean.getLogin());
        assertEquals("fallback-bean", dataBean.getName());
    }


    DeferredResult<ResponseEntity<DataBean>> runAync(Request<DataBean> request) {
        return asyncRestHystrixClient.execute(map -> {
            log.debug("response map: {}", map);
            ResponseBean responseBean = map.get("test-endpoint");
            assertEquals(HttpStatus.OK.value(), responseBean.statusCode());
            assertTrue(responseBean.endpointLatency() > -1);
            DataBean dataBean = responseBean.bean();
            return new ResponseEntity<>(dataBean, HttpStatus.OK);
        }, request);
    }

    //
    <T> Request<T> request(String path, Class<T> returnType) {
        return new Request.Builder<ProductBean>()
                .name("test-endpoint")
                .url(baseUrl + path)
                .returnType(returnType)
                .build();
    }

    @SneakyThrows
    <T> T get(DeferredResult<T> deferredResult) {
        for (int i = 0; i < 20; i++) {
            if (deferredResult.hasResult()) {
                log.debug("deferredResult: " + deferredResult.getResult());
                Object result = deferredResult.getResult();
                if (result instanceof Throwable) {
                    throw (Exception) result;
                }
                return (T) result;
            }
            Thread.sleep(100);
        }
        throw new IllegalStateException("Unable to retrieve deferred result, unhandled exception?, or too short wait time?");
    }
}
