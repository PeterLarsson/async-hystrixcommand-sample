package com.nmo.asynctest.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Peter on 2016-04-14.
 */
@RestController
@Slf4j
public class TestHeaderCompositeMicroService {

    public static int port;


    @Autowired
    AsyncRestHystrixClient asyncRestHystrixClient;

    String url() {
        return "http://localhost:" + ((port == 0) ? 8080 : port);
    }

    @RequestMapping("/header")
    public DeferredResult<ResponseEntity<HeaderBean>> header() {


        final Request<ProfileBean> profileBeanRequest =
                new Request.Builder<ProfileBean>()
                        .name("profile-service")
                        .url(url() + "/profiles/profileId")
                        .returnType(ProfileBean.class)
                        .build();

        final Request<ProductBean> productBeanRequest =
                new Request.Builder<ProductBean>()
                        .name("product-service")
                        .url(url() + "/products/productId")
                        .returnType(ProductBean.class)
                        .build();

        return execute0(profileBeanRequest, productBeanRequest);
    }


    @RequestMapping("/header/fail")
    public DeferredResult<ResponseEntity<HeaderBean>> headerFails() {

        final Request<ProfileBean> profileBeanRequest =
                new Request.Builder<ProfileBean>()
                        .name("profile-service")
                        .url(url() + "/profiles/error")
                        .returnType(ProfileBean.class)
                        .build();

        final Request<ProductBean> productBeanRequest =
                new Request.Builder<ProductBean>()
                        .name("product-service")
                        .url(url() + "/products/productId")
                        .returnType(ProductBean.class)
                        .build();

        return execute0(productBeanRequest, profileBeanRequest);
    }

    @RequestMapping("/header/timeout")
    public DeferredResult<ResponseEntity<HeaderBean>> headerTimeout() {

        final Request<ProfileBean> profileBeanRequest =
                new Request.Builder<ProfileBean>()
                        .name("profile-service")
                        .url(url() + "/profiles/timeout")
                        .returnType(ProfileBean.class)
                        .build();

        final Request<ProductBean> productBeanRequest =
                new Request.Builder<ProductBean>()
                        .name("product-service")
                        .url(url() + "/products/productId")
                        .returnType(ProductBean.class)
                        .build();

        return execute0(productBeanRequest, profileBeanRequest);
    }

    @RequestMapping("/header/fallbackRequestOnTimeout")
    public DeferredResult<ResponseEntity<HeaderBean>> headerFallbackOnTimeout() {

        final Request<ProfileBean> profileBeanRequest =
                new Request.Builder<ProfileBean>()
                        .name("profile-service")
                        .url(url() + "/profiles/timeout")
                        .returnType(ProfileBean.class)
                        .fallbackRequest(new Request.Builder<ProfileBean>()
                                .name("profile-service-fallback")
                                .url(url() + "/profiles/fallback")
                                .returnType(ProfileBean.class)
                                .fallbackValue(new ProfileBean())
                                .build())
                        .build();

        final Request<ProductBean> productBeanRequest =
                new Request.Builder<ProductBean>()
                        .name("product-service")
                        .url(url() + "/products/productId")
                        .returnType(ProductBean.class)
                        .build();

        return execute0(productBeanRequest, profileBeanRequest);
    }

    @RequestMapping("/header/fallbackRequestOnError")
    public DeferredResult<ResponseEntity<HeaderBean>> headerFallbackOnError() {

        final Request<ProfileBean> profileBeanRequest =
                new Request.Builder<ProfileBean>()
                        .name("profile-service")
                        .url(url() + "/profiles/error")
                        .returnType(ProfileBean.class)
                        .fallbackRequest(new Request.Builder<ProfileBean>()
                                .name("profile-service-fallback")
                                .url(url() + "/profiles/fallback")
                                .returnType(ProfileBean.class)
                                .build())
                        .build();

        final Request<ProductBean> productBeanRequest =
                new Request.Builder<ProductBean>()
                        .name("product-service")
                        .url(url() + "/products/productId")
                        .returnType(ProductBean.class)
                        .build();

        return execute0(productBeanRequest, profileBeanRequest);
    }

    @RequestMapping("/header/bulk")
    public DeferredResult<ResponseEntity<HeaderBean>> bulk() {

        List<Request<?>> requestList = new ArrayList<>(10);
        for (int i = 0; i < 10; i++) {
            final Request request = new Request.Builder<ProfileBean>()
                    .name("profile-service-" + i)
                    .url(url() + "/profiles/" + i)
                    .returnType(ProfileBean.class)
                    .build();
            requestList.add(request);
        }

        return execute0(requestList.toArray(new Request<?>[requestList.size()]));
    }


    //
    DeferredResult<ResponseEntity<HeaderBean>> execute0(final Request<?>... requestList) {
        return asyncRestHystrixClient.execute(stateMap -> {
            final HeaderBean headerBean = new HeaderBean();
            if (stateMap.containsKey("product-service")) {
                headerBean.setProductBean(stateMap.get("product-service").bean());
            }
            if (stateMap.containsKey("product-service-1")) {
                headerBean.setProductBean(stateMap.get("product-service-1").bean());
            }
            if (stateMap.containsKey("profile-service")) {
                headerBean.setProfileBean(stateMap.get("profile-service").bean());
            }
            if (stateMap.containsKey("profile-service-1")) {
                headerBean.setProfileBean(stateMap.get("profile-service-1").bean());
            }
            if (stateMap.containsKey("profile-service-fallback")) {
                headerBean.setProfileBean(stateMap.get("profile-service-fallback").bean());
            }

            return new ResponseEntity<HeaderBean>(headerBean, HttpStatus.OK);

        }, requestList);

    }

}
