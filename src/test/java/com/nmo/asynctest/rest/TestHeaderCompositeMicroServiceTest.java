package com.nmo.asynctest.rest;

import com.jayway.restassured.http.ContentType;
import com.nmo.asynctest.TestSupport;
import org.apache.http.HttpStatus;
import org.hamcrest.Matchers;
import org.junit.Test;

import static com.jayway.restassured.RestAssured.given;
import static com.jayway.restassured.RestAssured.when;

/**
 * Created by Peter on 2016-04-12.
 */
public class TestHeaderCompositeMicroServiceTest extends TestSupport {

    static DataBean fallbackData = new DataBean();

    static {
        fallbackData.setLogin("fallbackData");
    }

    @Test
    public void exampleOnLocalRestTestSuccess() {
        when().get("/500")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body("login", Matchers.is("login"));
    }

    @Test
    public void exampleOnLocalRestTestBadRequestTest() {
        when().get("/xxx")
                .then()
                .statusCode(HttpStatus.SC_BAD_REQUEST);
    }

    @Test
    public void successTest() {
        when().get("/header")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract().response().body().prettyPrint();
    }

    @Test
    public void failureTest() {
        given().accept(ContentType.JSON)
                .when().get("/header/fail")
                .then()
                .contentType(ContentType.JSON)
                .statusCode(HttpStatus.SC_INTERNAL_SERVER_ERROR)
                .body("status", Matchers.equalTo(500))
                .body("message", Matchers.notNullValue());
    }

    @Test
    public void fallbackRequestOnTimeoutTest() {
        when().get("/header/fallbackRequestOnTimeout")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract().response().body().prettyPrint();
    }

    @Test
    public void fallbackRequestOnErrorTest() {
        when().get("/header/fallbackRequestOnError")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract().response().body().prettyPrint();
    }

    @Test
    public void timeoutRequestTest() {
        when().get("/header/timeout")
                .then()
                .statusCode(HttpStatus.SC_INTERNAL_SERVER_ERROR)
                .extract().response().body().prettyPrint();
    }

    @Test
    public void bulkTest() {
        when().get("/header/bulk")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract().response().body().prettyPrint();
    }

}