package com.nmo.asynctest;

import com.jayway.restassured.RestAssured;
import com.nmo.asynctest.rest.AsyncRestHystrixClient;
import com.nmo.asynctest.rest.TestHeaderCompositeMicroService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * Created by Peter on 2016-04-17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = TestApplication.class)
@WebAppConfiguration
@org.springframework.boot.test.IntegrationTest({"server.port=0", "spring.cloud.config.enabled=false", "spring.cloud.bus.enabled=false", "spring.cloud.discovery.enabled=false"})
public abstract class TestSupport {

    @Autowired
    protected AsyncRestHystrixClient asyncRestHystrixClient;

    @Value("${local.server.port}")
    protected int port;

    protected String baseUrl;

    @Before
    public void before() {
        TestHeaderCompositeMicroService.port = RestAssured.port = port;
        this.baseUrl = "http://localhost:" + this.port + "/";
    }
}
