package com.nmo.asynctest.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ning.http.client.Response;
import lombok.Data;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;

/**
 * Asynchronous processing state for one endpoint call.
 */
@Data
@Slf4j
public class State implements ResponseBean {

    private static ObjectMapper objectMapper = new ObjectMapper();

    private String response;
    private Request request;
    private long created;
    private int responseStatusCode = 200;
    private int endpointLatency = -1;

    public State(final Request<?> request) {
        this.request = request;
        this.created = System.currentTimeMillis();
    }

    public long elasped() {
        return System.currentTimeMillis() - getCreated();
    }

    public State saveResponse(String response) {
        this.response = response;
        this.endpointLatency = (int) elasped();
        return this;
    }

    public String name() {
        return request.getName();
    }

    public boolean hasFallbackValue() {
        return request.getFallbackValue() != null;
    }

    public boolean hasFallbackRequest() {
        return request.getFallbackRequest() != null;
    }

    @Override
    public int endpointLatency() {
        return endpointLatency;
    }

    @Override
    public int statusCode() {
        return responseStatusCode;
    }

    @Override
    @SneakyThrows
    @SuppressWarnings("unchecked")
    public <T> T bean() {
        if (!isResponseOk(statusCode())) {
            throw new IllegalStateException("No valid response to return");
        }
        T bean = (T) objectMapper.readValue(getResponse(), request.getReturnType());
        log.debug("bean: {}",  bean);
        return bean;
    }

    @SneakyThrows
    public String fallbackValueAsString() {
        return hasFallbackValue() ? objectMapper.writeValueAsString(getRequest().getFallbackValue()) : null;
    }

    //
    public static boolean isResponseOk(final int statusCode) {
        return HttpStatus.valueOf(statusCode).is2xxSuccessful();
    }

    //
    public static boolean isResponseOk(final Response response) {
        return (response != null) && isResponseOk(response.getStatusCode());
    }
}
