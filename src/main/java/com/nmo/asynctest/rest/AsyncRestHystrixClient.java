package com.nmo.asynctest.rest;

import com.ning.http.client.AsyncHttpClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.async.DeferredResult;
import rx.Observable;
import rx.Subscription;

import java.util.Map;
import java.util.function.Function;

/**
 * REST and Hystrix client to be used by applications.
 */
@Component
@Slf4j
public class AsyncRestHystrixClient {

    @Autowired
    private AsyncHttpClient asyncHttpClient;

    @Value("${spring.application.name}")
    private String serviceName;

    //
    public <T> DeferredResult<ResponseEntity<T>> execute(final Function<Map<String, ? extends ResponseBean>, ResponseEntity<T>> function,
                                                         final Request<?>... requestList) {

        final long start = System.currentTimeMillis();

        final Observable<Map<String, ResponseBean>> observable =
                Observable.from(requestList)
                        .flatMap(req -> execute(new State(req)))
                .toMap(State::name, state -> state);

        final DeferredResult<ResponseEntity<T>> deferredResult = new DeferredResult<>();

        Subscription subscription = observable.subscribe(
                stateMap -> {
                    if (log.isDebugEnabled()) {
                        log.debug("Asynchronous processing completed in {} ms", (System.currentTimeMillis() - start));
                    }
                    deferredResult.setResult(function.apply(stateMap));
                },
                throwable -> {
                    log.error("Processing aborted", throwable);
                    if (throwable instanceof EndpointException) {
                        EndpointException ex = (EndpointException) throwable;
                        deferredResult.setErrorResult(ex.getErrorResponse());
                    } else {
                        deferredResult.setErrorResult(throwable);
                    }
                }
        );

        // Unsubscribe, i.e. tear down any resources setup during the processing
        deferredResult.onCompletion(() -> {
            log.debug("onCompletion: " + deferredResult.getResult());
            subscription.unsubscribe();
        });

        // Return to let go of the precious thread we are holding on to...
        log.debug("Non-blocking processing setup, return the request thread to the thread-pool");

        return deferredResult;
    }


    //
    Observable<State> execute(final State state) {
        log.debug("Request {} - {}", state.name(), state.getRequest().getUrl());
        return new AsyncHystrixRestCall(serviceName, state, asyncHttpClient)
                .toObservable()
                .doOnNext(r -> {
                    log.debug("Response {} - {}", state.name(), r);
                })
                .map(state::saveResponse)
                .onErrorResumeNext(t -> {
                    // last chance to recover from serious problems
                    if (state.hasFallbackValue()) {
                        return Observable.just(state);
                    } else {
                        state.setResponseStatusCode(500);
                        return Observable.error(new EndpointException(t, state.getRequest()));
                    }
                });
    }
}
