package com.nmo.asynctest.rest;

import lombok.Data;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.apache.commons.lang3.StringUtils.isEmpty;

/**
 * Abstacts a REST request.
 *
 * <p>
 *     Used by applications and supports fallback requests as well as values. For the time being is GET only requests supported.
 * </p>
 */
@Data
public class Request<R> {
    enum Method {
        GET
    }
    private String name;
    private String url;
    private Map<String, String> parameters = Collections.EMPTY_MAP;
    private Method method = Method.GET;
    private Class<R> returnType;
    private R fallbackValue;
    private Request<R> fallbackRequest;
    private int timeout = -1;

    public Map<String, String> getParameters() {
        if (this.parameters == Collections.EMPTY_MAP) {
            this.parameters = new HashMap<>();
        }
        return this.parameters;
    }

    //
    public static class Builder<T> {
        final Request<T> request = new Request<>();

        public Builder url(String url) {
            request.setUrl(url);
            return this;
        }

        public Builder name(String name) {
            request.setName(name);
            return this;
        }

        public Builder returnType(Class<T> returnType) {
            request.setReturnType(returnType);
            return this;
        }

        public Builder timeout(int millis) {
            request.setTimeout(millis);
            return this;
        }

        public Builder fallbackValue(T fallbackValue) {
            request.setFallbackValue(fallbackValue);
            return this;
        }

        public Builder parameter(String name, String value) {
            request.getParameters().put(name, value);
            return this;
        }

        public Builder fallbackRequest(Request<T> fallbackRequest) {
            request.setFallbackRequest(fallbackRequest);
            return this;
        }


        public Request build() {

            mandatory("Url", request.getUrl());
            if (request.getMethod() == Method.GET && request.getReturnType() == null) {
                mandatory("returnType", null);
            }
            if (request.getFallbackValue() != null && request.getFallbackRequest() != null) {
                throw new IllegalStateException("Either fallback request or value can be defined (mutually exclusive)");
            }
            return request;
        }

        static String mandatory(final String name, final String value) {
            if (isEmpty(value)) {
                throw new IllegalArgumentException(name + ": is missing");
            }
            return value;
        }
    }
}
