package com.nmo.asynctest.rest;

/**
 * The response provided to the consuming application.
 */
public interface ResponseBean {

    /**
     * Returns the total endpoint latency in millis.
     *
     * @return the endpoint latency in millis.
     */
    int endpointLatency();

    /**
     * Returns the HTTP status code.
     *
     * @return the HTTP status code.
     */
    int statusCode();

    /**
     * Returns the name, i.e. corresponds to the actual request name.
     *
     * @return the name (of the request).
     */
    String name();

    /**
     * Returns the bean acoording to the Request.returnType.
     *
     * @param <T> the returnType.
     * @return the bean.
     */
    <T> T bean();
}
