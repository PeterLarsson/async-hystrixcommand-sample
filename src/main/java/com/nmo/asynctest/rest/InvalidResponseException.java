package com.nmo.asynctest.rest;

import com.ning.http.client.Response;

/**
 * Thrown when the response is invalid.
 */
public class InvalidResponseException extends RuntimeException {

    private static final long serialVersionUID = 1L;
    private Response response;

    public InvalidResponseException(final Response response) {
        this.response = response;
    }

    public Response getResponse() {
        return response;
    }

}
