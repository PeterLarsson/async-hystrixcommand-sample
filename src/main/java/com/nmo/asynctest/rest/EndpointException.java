package com.nmo.asynctest.rest;

import com.google.common.base.Throwables;
import com.netflix.hystrix.exception.HystrixRuntimeException;
import com.ning.http.client.Response;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.concurrent.TimeoutException;

/**
 * Thrown when an connection error occurs (client or server).
 */
@Slf4j
public class EndpointException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private ResponseEntity<String> errorResponse;
    private Request<?> request;

    public EndpointException(final Throwable cause, final ResponseEntity<String> errorResponse, final Request<?> request) {
        super(cause);
        this.request =  request;
        this.errorResponse = errorResponse;
    }

    public EndpointException(final Throwable cause, final Request<?> request) {
        this(cause, null, request);
        this.errorResponse = createErrorResponse();
    }

    public ResponseEntity<String> getErrorResponse() {
        return errorResponse;
    }

    public String getUrl() {
        return request.getUrl();
    }

    public String getRequestName() {
        return request.getName();
    }

    @SneakyThrows
    ResponseEntity<String> createErrorResponse() {
        final Throwable cause = getCause();
        final Throwable rootCause = Throwables.getRootCause(cause);

        if (rootCause instanceof InvalidResponseException) {
            final Response r = ((InvalidResponseException) rootCause).getResponse();
            log.error("Error in response {}: {}", getRequestName(), r.getResponseBody());
            return new ResponseEntity<>(r.getResponseBody(), HttpStatus.valueOf(r.getStatusCode()));
        }

        HttpStatus httpStatus;
        String msg;
        if (cause instanceof TimeoutException) {
            httpStatus = HttpStatus.GATEWAY_TIMEOUT;
            msg = "Request timed out";
        } else if (cause instanceof HystrixRuntimeException) {
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            msg = "Reqeust cancelled by Hystrix (circuit breaker): " + rootCause;
        } else {
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            msg = "Request failed due to error: " + rootCause;
        }

        msg += " (HTTPStatus: " + httpStatus.value() + ")";
        log.error("Error while processing service \"{}\": {}", getRequestName(), msg);

        return new ResponseEntity<>(msg, httpStatus);
    }
}
