package com.nmo.asynctest.rest;

import com.netflix.hystrix.HystrixCommandGroupKey;
import com.netflix.hystrix.HystrixCommandKey;
import com.netflix.hystrix.HystrixObservableCommand;
import com.ning.http.client.*;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import rx.Observable;
import rx.Subscriber;

/**
 * Asynchronous and hystrix monitored endpoint processing.
 * <p>
 * Payload is JSON and represented as a String.
 * </p>
 *
 */
@Slf4j
public class AsyncHystrixRestCall extends HystrixObservableCommand<String> {
    private final State state;
    private final AsyncHttpClient asyncHttpClient;
    private final String serviceName;
    private Fallback fallback;


    // handle all fallbacks
    class Fallback {
        public Observable<String> get() {
            if (state.hasFallbackValue()) {
                return Observable.just(state.fallbackValueAsString());
            } if (state.hasFallbackRequest()) {
                state.setRequest(state.getRequest().getFallbackRequest());
                return new AsyncHystrixRestCall(serviceName, state, asyncHttpClient).toObservable();
            }
            return null;
        }
    }

    //
    public AsyncHystrixRestCall(final String serviceName, final State state, final AsyncHttpClient asyncHttpClient) {
        super(Setter.withGroupKey(HystrixCommandGroupKey.Factory.asKey(serviceName))
                .andCommandKey(HystrixCommandKey.Factory.asKey(state.name())));
        this.asyncHttpClient = asyncHttpClient;
        this.state = state;
        this.serviceName = serviceName;
    }


    @Override
    protected Observable<String> resumeWithFallback() {
        log.debug("fallback: {}", this.fallback);
        return (this.fallback == null) ? super.resumeWithFallback() : this.fallback.get();
    }

    @Override
    protected Observable<String> construct() {
        return Observable.create(new Observable.OnSubscribe<String>() {
            @Override
            public void call(final Subscriber<? super String> observer) {
                try {
                    doEndpointAsync(state.getRequest(), observer);
                } catch (Throwable throwable) {
                    log.error("doEndpointAsync", throwable);
                    observer.onError(new EndpointException(throwable, state.getRequest()));
                }
            }
        });
    }

    //
    void doEndpointAsync(final Request<?> request, final Subscriber<? super String> observer) {

        if (state.hasFallbackRequest() || state.hasFallbackValue()) {
            this.fallback = new Fallback();
        }

        log.debug("doEndpointAsync: {}", request);
        if (!observer.isUnsubscribed()) {
            getClientBuilder(request).execute(new AsyncCompletionHandler<Response>() {
                @Override
                public Response onCompleted(Response response) throws Exception {
                    if (State.isResponseOk(response)) {
                        observer.onNext(response.getResponseBody());
                        observer.onCompleted();
                        return response;
                    } else {
                        throw new InvalidResponseException(response);
                    }
                }

                @Override
                @SneakyThrows
                public STATE onStatusReceived(final HttpResponseStatus status) {
                    if (log.isDebugEnabled()) {
                        log.debug("onStatusReceived: HTTP {}", status.getStatusCode());
                    }
                    state.setResponseStatusCode(status.getStatusCode());
                    return super.onStatusReceived(status);
                }

                @Override
                public void onThrowable(final Throwable throwable) {
                    log.error("onThrowable: {}: ", throwable.toString(), throwable);
                    final EndpointException ex = new EndpointException(throwable, request);
                    observer.onError(ex);
                }
            });
        }
    }

    /**
     * Returns HTTP Client builder.
     *
     * <p>
     *     Sets up all HTTP connection stuff.
     * </p>
     *
     * @param request the request.
     * @return the builder.
     */
    AsyncHttpClient.BoundRequestBuilder getClientBuilder(final Request<?> request) {
        final AsyncHttpClient.BoundRequestBuilder builder = asyncHttpClient.prepareGet(request.getUrl());
        request.getParameters().forEach((key, value) -> builder.addQueryParam(key, value));
        if (request.getTimeout() > 0) {
            builder.setRequestTimeout(request.getTimeout());
        }
        return builder;
    }
}
