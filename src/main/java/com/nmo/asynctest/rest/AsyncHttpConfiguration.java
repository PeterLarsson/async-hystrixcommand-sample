package com.nmo.asynctest.rest;

import com.ning.http.client.AsyncHttpClient;
import com.ning.http.client.AsyncHttpClientConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Configures HTTP and REST client.
 *
 */
@Configuration
@EnableHystrix
@Slf4j
public class AsyncHttpConfiguration {

    // default timeout is greater than hystrix default, and this is important
    // in  order to be able to test different kinds of timeouts (user-defined, hystrix-defined, server-side).
    @Value("${timeout:1200}")
    int timeout;
    @Value("${retries:2}")
    int retries;

    @Bean
    public AsyncHttpClient asyncHttpClient() {
        log.debug(
                "Creates a new AsyncHttpClient Bean with: connection-timeout: {} ms, " +
                        "request-timeout: {} ms and max-retries: {}",
                timeout,
                timeout,
                retries);

        final AsyncHttpClientConfig config = new AsyncHttpClientConfig.Builder()
                .setConnectTimeout(timeout)
                .setRequestTimeout(timeout)
                .setMaxRequestRetry(retries)
                .build();

        return new AsyncHttpClient(config);
    }

    @Bean
    public AsyncRestHystrixClient asyncRestHystrixClient() {
        log.debug("Creates a new AsyncRestHystrixClient Bean");
        return new AsyncRestHystrixClient();
    }
}
